##------------------------------------------------------------##
## MS-Windows-AD-Objects - Integration Macros - Start Section ##
##------------------------------------------------------------##
## AD Searches - Replace ldapsearch with local Lookup
### All of the Searches below had the referenced ldapsearch replaced with doing a local csv lookup
### that contains the needed AD Ldap Data.

#############################################################################################
## Computer Search Macros that point to AD_Computer_LDAP_list Lookup:
#############################################################################################
[secrpt-active-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list \
| search domain="$domain$" \
|  table sAMAccountName,cn,dNSHostName,operatingSystem,operatingSystemServicePack,userAccountControl \
|join type=outer sAMAccountName [search eventtype=msad-successful-computer-logons dest_nt_domain="$domain$"|stats max(_time) as lastLogonTime by user|rename user as sAMAccountName]\
| where isnull(lastLogonTime)\
| makemv delim=":" uac_details\
| table cn,dNSHostName,uac_details,operatingSystem,operatingSystemServicePack,userAccountControl\
| rename cn as Computer,operatingSystem as "Operating System",operatingSystemServicePack as "Service Pack"
args = domain
## Replaced Definition - definition = eventtype=msad-successful-computer-logons dest_nt_domain="$domain$"|stats max(_time) as lastLogonTime by dest_nt_domain,user|ldapfilter domain=$domain$ search="(&(objectClass=computer)(sAMAccountName=$user$))" attrs="cn,dNSHostName,operatingSystem,operatingSystemServicePack"|eval lastLogonTime=strftime(lastLogonTime,"%c")|table cn,dNSHostName,operatingSystem,operatingSystemServicePack,lastLogonTime|rename cn as Computer,operatingSystem as "Operating System",operatingSystemServicePack as "Service Pack",lastLogonTime as "Last Logon Time"

[secrpt-all-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list \
| search domain="$domain$"\
| makemv delim=":" uac_details\
| sort cn\
| table cn,dNSHostName,uac_details,operatingSystem,operatingSystemServicePack,whenChanged,whenCreated
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(objectClass=computer)"|sort cn|table cn,dNSHostName,userAccountControl,operatingSystem,operatingSystemServicePack

[secrpt-all-domain-controllers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list \
| search domain="$domain$" primaryGroupID=516\
| makemv delim=":" uac_details\
| sort cn\
| table cn,dNSHostName,uac_details,operatingSystem,operatingSystemServicePack,whenChanged,whenCreated
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectClass=computer)(userAccountControl:1.2.840.113556.1.4.803:=532480))"|sort cn|table cn,dNSHostName,userAccountControl,operatingSystem,operatingSystemServicePack

[secrpt-deleted-computers(1)]
args = domain
definition = `ms_ad_obj_computer_changes_action("deleted")` dest_nt_domain="$domain$"\
|eval adminuser=src_nt_domain."\\".src_user\
|table _time,user,adminuser\
|rename user as "Deleted Computer",adminuser as "Deleted By"
## Replaced Definition - definition = eventtype=wineventlog_security (EventCode=647 OR EventCode=4743) dest_nt_domain="$domain$"|eval adminuser=src_nt_domain."\\".src_user|table _time,user,adminuser|rename user as "Deleted Computer",adminuser as "Deleted By"

[secrpt-disabled-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Disabled")) \
| search uac_filter=*\
| sort sAMAccountName\
| makemv delim=":" uac_details\
| table cn,dNSHostName,uac_details,userAccountControl,whenChanged
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectClass=computer)(userAccountControl:1.2.840.113556.1.4.803:=2))"|sort sAMAccountName|table cn,dNSHostName,userAccountControl,whenChanged

[secrpt-inactive-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list \
| search domain="$domain$" \
| table sAMAccountName,cn,dNSHostName,operatingSystem,operatingSystemServicePack,userAccountControl \
| join type=outer sAMAccountName [search eventtype=msad-successful-computer-logons dest_nt_domain="$domain$"|stats max(_time) as lastLogonTime by user|rename user as sAMAccountName]\
| where isnull(lastLogonTime)\
| makemv delim=":" uac_details\
| table cn,dNSHostName,uac_details,operatingSystem,operatingSystemServicePack,userAccountControl\
| rename cn as Computer,operatingSystem as "Operating System",operatingSystemServicePack as "Service Pack"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=computer)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))" attrs="sAMAccountName,cn,dNSHostName,operatingSystem,operatingSystemServicePack,userAccountControl"|join type=outer sAMAccountName [search eventtype=msad-successful-computer-logons dest_nt_domain="$domain$"|stats max(_time) as lastLogonTime by user|rename user as sAMAccountName]|where isnull(lastLogonTime)|table cn,dNSHostName,operatingSystem,operatingSystemServicePack,userAccountControl|rename cn as Computer,operatingSystem as "Operating System",operatingSystemServicePack as "Service Pack"

[secrpt-new-computers(1)]
args = domain
definition = `ms_ad_obj_computer_changes_action("created")` dest_nt_domain="$domain$"\
| table _time,src_user,src_nt_domain,dest_nt_domain,user\
| eval adminuser=src_nt_domain."\\".src_user\
| eval sAMAccountName=$user$ \
| join sAMAccountName [|inputlookup AD_Computer_LDAP_list | search sAMAccountName=$user$ | table dNSHostName,operatingSystem,operatingSystemServicePack]\
| table _time,cn,dNSHostName,operatingSystem,operatingSystemServicePack,adminuser\
| rename cn as "Added Computer",operatingSystem as "Operating System",operatingSystemServicePack as "ServicePack",adminuser as "Added By"
## Replaced Definition - definition = eventtype=wineventlog_security (EventCode=645 OR EventCode=4741) dest_nt_domain="$domain$"|table _time,src_user,src_nt_domain,dest_nt_domain,user|eval adminuser=src_nt_domain."\\".src_user|ldapfilter domain=$domain$ search="(&(objectClass=computer)(sAMAccountName=$user$))" attrs="cn,dNSHostName,operatingSystem,operatingSystemServicePack"|table _time,cn,dNSHostName,operatingSystem,operatingSystemServicePack,adminuser|rename cn as "Added Computer",operatingSystem as "Operating System",operatingSystemServicePack as "ServicePack",adminuser as "Added By"

[secrpt-trusted-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Server Trust Account|Workstation Trust Account")) \
| search uac_filter=* \
| makemv delim=":" uac_details\
| table cn,dNSHostName,uac_details,userAccountControl,operatingSystem,operatingSystemServicePack
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectClass=computer)(userAccountControl:1.2.840.113556.1.4.803:=524288))"|table cn,dNSHostName,userAccountControl,operatingSystem,operatingSystemServicePack

[secrpt-unmanaged-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list WHERE domain="$domain$" AND NOT managedBy=""\
| sort sAMAccountName\
| makemv delim=":" uac_details\
| table cn,dNSHostName,uac_details, operatingSystem,operatingSystemServicePack
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectClass=computer)(!(managedBy=*)))"|sort sAMAccountName|table cn,dNSHostName,userAccountControl,operatingSystem,operatingSystemServicePack

[secrpt-managed-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list WHERE domain="$domain$" AND managedBy="*" AND NOT managedBy=""\
| sort sAMAccountName\
| makemv delim=":" uac_details\
| table cn,dNSHostName,managedBy,uac_details, operatingSystem,operatingSystemServicePack

[secrpt-unused-computers(1)]
args = domain
definition = inputlookup AD_Computer_LDAP_list \
| search domain="$domain$" logonCount=0\
| makemv delim=":" uac_details\
| table cn,dNSHostName,uac_details, userAccountControl,operatingSystem,operatingSystemServicePack
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectClass=computer)(|(!(logonCount=*))(logonCount=0)))"|table cn,dNSHostName,userAccountControl

#############################################################################################
## Users Search Macros that point to AD_User_LDAP_list Lookup:
#############################################################################################
[secrpt-all-users(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true\
| search domain="$domain$"\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer)))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-active-users(1)]
args = domain
definition = eventtype=msad-successful-user-logons dest_nt_domain="$domain$"  sourcetype="WinEventLog:Security"\
| fields _time, dest_nt_domain,user\
| stats  max(_time) as lastLogonTime by dest_nt_domain,user\
| join  user  [|inputlookup AD_User_LDAP_list | search domain="$domain$" | rename sAMAccountName AS user | fields cn,user,userPrincipalName]\
| eval  lastLogonTime=strftime(lastLogonTime,"%c")\
| stats last(lastLogonTime) AS lastLogonTime by user,cn,userPrincipalName\
| rename  user as Username,cn as "Full Name", lastLogonTime as "Last Logon Time"
## Replaced Definition - definition = eventtype=msad-successful-user-logons dest_nt_domain="$domain$"|stats max(_time) as lastLogonTime by dest_nt_domain,user|ldapfilter domain=$domain$ search="(&(objectClass=user)(sAMAccountName=$user$))" attrs="cn,userPrincipalName"|eval lastLogonTime=strftime(lastLogonTime,"%c")|table user,cn,userPrincipalName,lastLogonTime|rename user as Username,cn as "Full Name", lastLogonTime as "Last Logon Time"

[secrpt-deleted-users(1)]
args = domain
definition = `ms_ad_obj_user_changes_action("deleted")` dest_nt_domain="$domain$"|eval adminuser=src_nt_domain."\\".src_user|table _time,user,adminuser|rename user as "Deleted User", adminuser as "Deleted By"

[secrpt-deleted-users_ms_ad_obj(3)]
definition = inputlookup AD_User_LDAP_list WHERE domain="$domain$" AND isDeleted="TRUE"\
| eval begintime=strptime("$starttime$", "%m/%d/%y %I:%M %P")\
| eval finishtime=strptime("$endtime$", "%m/%d/%y %I:%M %P")\
| eval whenDeleted=strftime(deletedDate, "%I:%M.%S %P, %a %m/%d/%Y")\
| where deletedDate>begintime AND deletedDate<finishtime\
| sort sAMAccountName\
| `ms_ad_obj_uac_bin_fields`\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, whenDeleted, whenCreated,whenChanged,userPrincipalName,userAccountControl, uac_details,domain, distinguishedName\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
args = domain,starttime,endtime
iseval = 0

[secrpt-disabled-users(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Disabled")) \
| search uac_filter=*\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(userAccountControl:1.2.840.113556.1.4.803:=2))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl,whenChanged

[secrpt-inactive-users(1)]
args = domain
definition = inputlookup AD_User_LDAP_list\
| search domain="$domain$"\
| fields domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated,deletedDate\
| join type=outer sAMAccountName [search eventtype=msad-successful-user-logons dest_nt_domain="$domain$"| fields _time, user|stats max(_time) AS lastLogonTime by user|rename user as sAMAccountName | fields sAMAccountName, lastLogonTime]\
| where isnull(lastLogonTime)\
| `ms_ad_obj_uac_bin_fields`\
| makemv delim=":" uac_details\
| sort sAMAccountName\
| eval whenDeleted=strftime(deletedDate, "%I:%M.%S %P, %a %m/%d/%Y")\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated,whenDeleted\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectclass=computer))(!(userAccountControl:1.2.840.113556.1.4.803:=2)))" attrs="sAMAccountName,cn,userPrincipalName,userAccountControl"|join type=outer sAMAccountName [search eventtype=msad-successful-user-logons dest_nt_domain="$domain$"|stats max(_time) as lastLogonTime by user|rename user as sAMAccountName]|where isnull(lastLogonTime)|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-new-users(1)]
args = domain
definition = `ms_ad_obj_user_changes_action("created")` dest_nt_domain="$domain$"|eval adminuser=src_nt_domain."\\".src_user|table _time,user,adminuser|rename user as "Added User", adminuser as "Added By"

[secrpt-new-users_ms_ad_obj(3)]
definition = inputlookup AD_User_LDAP_list WHERE domain="$domain$"\
| eval begintime=strptime("$starttime$", "%m/%d/%y %I:%M %P")\
| eval finishtime=strptime("$endtime$", "%m/%d/%y %I:%M %P")\
| eval whenCreated_epoch=strptime(whenCreated, "%I:%M.%S %P, %a %m/%d/%Y")\
| where whenCreated_epoch>begintime AND whenCreated_epoch<finishtime\
| sort sAMAccountName\
| `ms_ad_obj_uac_bin_fields`\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, whenCreated, whenChanged,userPrincipalName,userAccountControl, uac_details,domain, distinguishedName\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
args = domain,starttime,endtime
iseval = 0

[secrpt-sensitive-users(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Sensitive - Not Delegated"))\
| search uac_filter=*\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(userAccountControl:1.2.840.113556.1.4.803:=1048576))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl


[secrpt-unused-users(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" AND logonCount=0\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(|(!(logonCount=*))(logonCount=0)))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-users-no-smartcard-required(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Password Not Required"))\
| search NOT uac_filter=*\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(!(userAccountControl:1.2.840.113556.1.4.803:=262144)))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-users-password-too-old(1)]
args = domain
definition = inputlookup AD_User_LDAP_list WHERE domain="$domain$"\
| join type=outer sAMAccountName [search eventtype=msad-password-changes dest_nt_domain=$domain$|stats max(_time) as maxtime by user|rename user as sAMAccountName|where isnull(maxtime)]\
| `ms_ad_obj_uac_bin_fields`\
| makemv delim=":" uac_details\
| table sAMAccountName,cn,userPrincipalName,userAccountControl,uac_details,pwdLastSet
## Replaced Definition - definition = ldapsearch domain=$domain$ search="(&(objectclass=user)(!(objectclass=computer)))"|join type=outer sAMAccountName [search eventtype=msad-password-changes dest_nt_domain=$domain$|stats max(_time) as maxtime by user|rename user as sAMAccountName]|where isnull(maxtime)|table sAMAccountName,cn,userPrincipalName,userAccountControl,pwdLastSet

[secrpt-users-smartcard-required(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Smart Card Required"))\
| search uac_filter=*\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(userAccountControl:1.2.840.113556.1.4.803:=262144))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-users-that-dont-expire(1)]
args = domain
definition = inputlookup AD_User_LDAP_list WHERE domain="*" AND accountExpires=0 OR accountExpires="Never Expires"\
| makemv delim=":" uac_details\
| table domain, accountExpires, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(accountExpires=9223372036854775807))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-users-that-dont-require-password(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Password Not Required")) \
| search uac_filter=*\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName, userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(userAccountControl:1.2.840.113556.1.4.803:=32))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-users-whose-password-doesnt-expire(1)]
args = domain
definition = inputlookup AD_User_LDAP_list append=true WHERE domain="$domain$" \
| eval uac_filter=mvfilter(match(uac_details, "Password Does Not Expire"))\
| search uac_filter=*\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(userAccountControl:1.2.840.113556.1.4.803:=65536))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

[secrpt-users-with-no-manager(1)]
args = domain
definition = [ms_ad_obj_secrpt-users-with-no-manager(1)]
definition = inputlookup AD_User_LDAP_list WHERE domain="$domain$" AND NOT manager="*"\
| sort sAMAccountName\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details

[secrpt-users-with-manager(1)]
definition = inputlookup AD_User_LDAP_list WHERE domain="$domain$" AND manager="*" AND NOT manager=""\
| sort sAMAccountName\
| makemv delim=":" uac_details\
| table domain, sAMAccountName, userPrincipalName,userAccountControl, uac_details,domain, distinguishedName,whenChanged,whenCreated\
| sort sAMAccountName\
| rename sAMAccountName AS "user", uac_details AS userAccountControl_Details
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=user)(!(objectClass=computer))(!(manager=*)))"|sort sAMAccountName|table sAMAccountName,cn,userPrincipalName,userAccountControl

#############################################################################################
## Group Policies Search Macros that point to AD_GroupPolicies_LDAP_list Lookup:
#############################################################################################
[secrpt-all-group-policies(1)]
args = domain
definition = inputlookup AD_GroupPolicies_LDAP_list\
| search domain="$domain$"\
| eval linkedContainers=split(lc,"####")\
| table cn,displayName,versionNumber,linkedContainers\
| sort cn\
| rename cn as "Group Policy ID",displayName as "Group Policy Name",versionNumber as "Version",linkedContainers as "Linked Containers"
## Replaced Definition - definition = ldapsearch domain=$domain$ search="(objectClass=groupPolicyContainer)" attrs="displayName,cn,versionNumber"|join type=outer cn [ldapsearch domain=$domain$ search="(gPLink=*)" attrs="distinguishedName,gPLink"|where isnotnull(gPLink)|rex field=gPLink max_match=10 "\[LDAP://(CN|cn)=(?<cn>[^,]+),"|table cn,distinguishedName|mvexpand cn|mvcombine distinguishedName|eval lc=mvjoin(distinguishedName,"###")|fields cn,lc]|eval linkedContainers=split(lc,"###")|table cn,displayName,versionNumber,linkedContainers|rename cn as "Group Policy ID",displayName as "Group Policy Name",versionNumber as "Version",linkedContainers as "Linked Containers"

[secrpt-deleted-group-policies(1)]
args = domain
definition = eventtype=ms_ad_obj_msad_data  (admonEventType=Deleted) objectClass="top|container|groupPolicyContainer"\
| eval deletedDate=if(match(lower(admonEventType), "deleted") OR match(lower(isDeleted), "true"), strptime(whenChanged, "%I:%M.%S %p, %a %m/%d/%Y"), 0)\
| fillnull value=""\
| stats max(deletedDate) as deletedDate, first(cn) as cn,first(uSNChanged) as uSNChanged, first(instanceType) as instanceType, first(lastKnownParent) as lastKnownParent, first(whenChanged) as whenChanged by distinguishedName, objectGUID,isDeleted,isRecycled\
| join objectGUID [|inputlookup AD_GroupPolicies_LDAP_list | table objectGUID,src_nt_domain,displayName,versionNumber]\
| eval When_Deleted=strftime(deletedDate,"%m/%d/%y %H:%M:%S")\
| table displayName,src_nt_domain,When_Deleted,cn\
| sort cn\
| rename cn as "Group Policy ID",displayName as "Group Policy Name",src_nt_domain AS "Group Policy Domain"
## Replaced Definition - definition = eventtype=admon-gpo isDeleted=TRUE|lookup HostToDomain host|search src_nt_domain="$domain$"|table _time,cn|dedup cn

[secrpt-disabled-group-policies(1)]
args = domain
definition = inputlookup AD_GroupPolicies_LDAP_list WHERE domain="$domain$" AND NOT flags="0" AND isDeleted="TRUE"\
| eval whenDeleted=strftime(deletedDate, "%I:%M.%S %P, %a %m/%d/%Y")\
| eval whenChanged_epoch=strptime(whenChanged, "%I:%M.%S %P, %a %m/%d/%Y")\
| sort whenChanged_epoch\
| eval Status=case(flags==1,"User Settings Disabled",flags==2,"Computer Settings Disabled",flags==3,"All Settings Disabled",flags==0,"Enabled")\
| table cn,displayName,versionNumber,flags,Status,whenChanged,deleted_flg, whenDeleted\
| rename cn as "Group Policy ID",displayName as "Group Policy Name",versionNumber as "Version", deleted_flg AS "Is Deleted"
## Replaced Definition - definition = ldapsearch domain=$domain$ search="(&(objectClass=groupPolicyContainer)(!(flags=0)))" attrs="displayName,cn,versionNumber,flags,whenChanged"|join type=outer cn [ldapsearch domain=$domain$ search="(gPLink=*)" attrs="distinguishedName,gPLink"|where isnotnull(gPLink)|rex field=gPLink max_match=10 "\[LDAP://(CN|cn)=(?<cn>[^,]+),"|table cn,distinguishedName|mvexpand cn|mvcombine distinguishedName|eval lc=mvjoin(distinguishedName,"###")|fields cn,lc]|eval linkedContainers=split(lc,"###")|eval Status=case(flags==1,"User Settings Disabled",flags==2,"Computer Settings Disabled",flags==3,"All Settings Disabled",flags==0,"Enabled")|table cn,displayName,versionNumber,Status,,whenChanged,linkedContainers|rename cn as "Group Policy ID",displayName as "Group Policy Name",versionNumber as "Version",linkedContainers as "Linked Containers"

[secrpt-new-group-policies(1)]
args = domain
definition = eventtype=admon-gpo|where uSNCreated==uSNChanged|lookup HostToDomain host|search src_nt_domain="$domain$"|dedup distinguishedName|table _time,cn,distinguishedName|join type=outer cn [ldapsearch domain=$domain$ search="(gPLink=*)" attrs="distinguishedName,gPLink"|where isnotnull(gPLink)|rex field=gPLink max_match=10 "\[LDAP://(CN|cn)=(?<cn>[^,]+),"|table cn,distinguishedName|mvexpand cn|mvcombine distinguishedName|eval lc=mvjoin(distinguishedName,"###")|fields cn,lc]|ldapfetch attrs="displayName,versionNumber"|lookup ActiveDirectory_GPOInfoLookup distinguishedName OUTPUT displayName, deletedDate | `format-ad-object-displayname(displayName,deletedDate)`|eval linkedContainers=split(lc,"###")|table _time,cn,displayName,versionNumber,linkedContainers
## Replaced Definition - definition = eventtype=admon-gpo|where uSNCreated==uSNChanged|lookup HostToDomain host|search src_nt_domain="$domain$"|dedup distinguishedName|table _time,cn,distinguishedName|join type=outer cn [ldapsearch domain=$domain$ search="(gPLink=*)" attrs="distinguishedName,gPLink"|where isnotnull(gPLink)|rex field=gPLink max_match=10 "\[LDAP://(CN|cn)=(?<cn>[^,]+),"|table cn,distinguishedName|mvexpand cn|mvcombine distinguishedName|eval lc=mvjoin(distinguishedName,"###")|fields cn,lc]|ldapfetch attrs="displayName,versionNumber"|lookup ActiveDirectory_GPOInfoLookup distinguishedName OUTPUT displayName, deletedDate | `format-ad-object-displayname(displayName,deletedDate)`|eval linkedContainers=split(lc,"###")|table _time,cn,displayName,versionNumber,linkedContainers

[secrpt-new-group-policies_ms_ad_obj(3)]
definition = inputlookup AD_GroupPolicies_LDAP_list WHERE domain="$domain$"\
| eval begintime=strptime("$starttime$", "%m/%d/%y %I:%M %P")\
| eval finishtime=strptime("$endtime$", "%m/%d/%y %I:%M %P")\
| eval whenCreated_epoch=strptime(whenCreated, "%I:%M.%S %P, %a %m/%d/%Y")\
| where whenCreated_epoch>begintime AND whenCreated_epoch<finishtime\
| rex field=cn "(?<gpo>\{.*\})"\
| join type=left gpo [| inputlookup AD_OU_LDAP_list WHERE domain="$domain$"\
| search gpo=* NOT gpo=""\
| makemv delim="####" gpo\
| mvexpand gpo\
| eval ou_linked="####".ou." (".distinguishedName.")"\
| stats values(ou_linked) AS ou_linked by gpo\
| table gpo,ou_linked]\
| makemv delim="####" ou_linked\
| table displayName,whenCreated,gpo,versionNumber,ou_linked\
| rename gpo_displayName AS "Group Policy", gpo as "GPO_ID",ou_linked as "Linked OU"
args = domain,starttime,endtime
iseval = 0

#############################################################################################
## Groups Search Macros that point to AD_Groups_LDAP_list Lookup:
#############################################################################################
[secrpt-all-groups(1)]
args = domain
definition = inputlookup AD_Groups_LDAP_list\
| search domain="$domain$"\
| sort cn\
| makemv delim="####" member \
| eval membercount=mvcount(member)\
| eval membercount=if(membercount=="",0,membercount)\
| table cn,groupType_Name,membercount,whenChanged,whenCreated\
| rename cn as "Group Name",groupType_Name as "Type",membercount as "# Members"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=group)(groupType:1.2.840.113556.1.4.803:=2147483648))" attrs="cn,distinguishedName,primaryGroupToken,member,groupType,systemFlags"|ldapgroup|sort cn|table cn,groupType,member_dn,member_type

[secrpt-changed-groups(1)]
args = domain
definition = inputlookup AD_Groups_LDAP_list\
| search domain="$domain$" NOT isDeleted="TRUE"\
| eval begintime=strptime("$starttime$", "%m/%d/%y %I:%M %P")\
| eval finishtime=strptime("$endtime$", "%m/%d/%y %I:%M %P")\
| eval whenChanged_epoch=strptime(whenChanged, "%I:%M.%S %P, %a %m/%d/%Y")\
| where whenChanged_epoch>begintime AND whenChanged_epoch<finishtime\
| sort sAMAccountName\
| makemv delim="####" memberOf\
| table sAMAccountName,whenChanged,distinguishedName,groupType_Name,memberOf,whenCreated\
| rename sAMAccountName as "Group_Name",groupType_Name as "Type"
## Replaced Definition - definition = eventtype=wineventlog_security (EventCode=668 OR EventCode=4764) dest_nt_domain="$domain$"|eval adminuser=src_nt_domain."\\".src_user|lookup GroupType MSADGroupClassID OUTPUT MSADGroupClass|lookup GroupType MSADGroupClassID AS MSADNewGroupClassID OUTPUT MSADGroupClass AS MSADNewGroupClass|table _time,signature,user_group,adminuser,MSADGroupClass,MSADGroupType,MSADNewGroupClass,MSADNewGroupType|rename signature as "Action",user_group as "Group Name",adminuser as "Changed By",MSADGroupClass as "Old Class",MSADGroupType as "Old Type",MSADNewGroupClass as "New Class",MSADNewGroupType as "New Type"

[secrpt-deleted-groups(1)]
args = domain
definition = `ms_ad_obj_group_changes_action("deleted")` dest_nt_domain="$domain$"|lookup GroupType MSADGroupClassID OUTPUT MSADGroupClass|eval adminuser=src_nt_domain."\\".src_user|table _time,user_group,MSADGroupClass,MSADGroupType,adminuser|rename user_group as "Group Name",MSADGroupClass as "Class",MSADGroupType as "Type",adminuser as "Deleted By"

[secrpt-deleted-groups_ms_ad_obj(3)]
definition = inputlookup AD_Groups_LDAP_list WHERE domain="$domain$" AND isDeleted="TRUE"\
| eval begintime=strptime("$starttime$", "%m/%d/%y %I:%M %P")\
| eval finishtime=strptime("$endtime$", "%m/%d/%y %I:%M %P")\
| eval whenDeleted=strftime(deletedDate, "%I:%M.%S %P, %a %m/%d/%Y")\
| where deletedDate>begintime AND deletedDate<finishtime\
| sort sAMAccountName\
| makemv delim="####" memberOf\
| table sAMAccountName,whenDeleted,distinguishedName,groupType_Name,memberOf,whenCreated,whenChanged\
| rename sAMAccountName as "Group_Name",groupType_Name as "Type"
args = domain,starttime,endtime
iseval = 0

[secrpt-empty-groups(1)]
args = domain
definition = inputlookup AD_Groups_LDAP_list WHERE membercount=0 AND domain="$domain$"\
| sort cn\
| table cn,groupType,groupType_Name,whenChanged,whenCreated\
| rename cn as "Group Name",groupType_Name as "Type"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=group)(groupType:1.2.840.113556.1.4.803:=2147483648))" attrs="cn,distinguishedName,primaryGroupToken,member,groupType,systemFlags"|ldapgroup|where isnull(member_dn)|sort cn|table cn,groupType|rename cn as "Group Name",groupType as "Type"

[secrpt-large-groups(2)]
args = domain,minsize
definition = inputlookup AD_Groups_LDAP_list WHERE membercount>$minsize$ AND domain="$domain$"\
| sort -membercount, cn\
| table cn,groupType_Name,membercount,whenChanged,whenCreated\
| rename cn as "Group Name",groupType_Name as "Type",membercount as "# Members"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=group)(groupType:1.2.840.113556.1.4.803:=2147483648))" attrs="cn,distinguishedName,primaryGroupToken,member,groupType,systemFlags"|ldapgroup|eval membercount=if(isnull(member_dn),0,mvcount(member_dn))|where membercount>$minsize$|sort cn|table cn,groupType,membercount,member_dn,member_type|rename cn as "Group Name",groupType as "Type",membercount as "# Members",member_dn as "Member DN",member_type as "Member Type"

[secrpt-nested-groups(1)]
args = domain
definition = inputlookup AD_Groups_LDAP_list WHERE domain="$domain$" AND NOT memberOf=""\
| makemv delim="####" memberOf\
| table distinguishedName,cn,groupType_Name,memberOf,whenChanged,whenCreated\
| rename cn as "Group Name",groupType_Name as "Type"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=group)(groupType:1.2.840.113556.1.4.803:=2147483648)(member=*))" attrs="distinguishedName,cn,groupType,member"|ldapgroup|mvexpand member_type|search member_type="NESTED*"|dedup cn,member_type|sort distinguishedName|transaction distinguishedName|table distinguishedName,cn,groupType,member_type

[secrpt-new-groups(1)]
args = domain
definition = `ms_ad_obj_group_changes_action("created")` dest_nt_domain="$domain$"\
| lookup GroupType MSADGroupClassID OUTPUT MSADGroupClass\
| eval adminuser=src_nt_domain."\\".src_user\
| table _time,user_group,MSADGroupClass,MSADGroupType,adminuser\
| rename user_group as "Group Name",MSADGroupClass as "Class",MSADGroupType as "Type",adminuser as "Added By" 
## Replaced Definition - definition = eventtype=wineventlog_security (EventCode=631 OR EventCode=635 OR EventCode=658 OR EventCode=4727 OR EventCode=4731 OR EventCode=4754) dest_nt_domain="$domain$"|lookup GroupType MSADGroupClassID OUTPUT MSADGroupClass|eval adminuser=src_nt_domain."\\".src_user|table _time,user_group,MSADGroupClass,MSADGroupType,adminuser|rename user_group as "Group Name",MSADGroupClass as "Class",MSADGroupType as "Type",adminuser as "Added By"

[secrpt-unmanaged-groups(1)]
args = domain
definition = inputlookup AD_Groups_LDAP_list WHERE domain="$domain$" managedBy=""\
| sort cn\
| eval membercount=if(membercount=="",0,membercount)\
| table cn,groupType_Name,membercount,whenChanged,whenCreated\
| rename cn as "Group Name",groupType_Name as "Type",membercount as "# Members"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(objectclass=group)(!(manager=*))(groupType:1.2.840.113556.1.4.803:=2147483648))" attrs="cn,distinguishedName,primaryGroupToken,member,groupType,systemFlags"|ldapgroup|sort cn|table cn,groupType,member_dn,member_type|rename cn as "Group Name",groupType as "Type",member_dn as "Member DN",member_type as "Member Type"

[secrpt-managed-groups(1)]
args = domain
definition = inputlookup AD_Groups_LDAP_list WHERE domain="$domain$" AND managedBy="*" AND NOT managedBy=""\
| sort cn\
| eval membercount=if(membercount=="",0,membercount)\
| table cn,managedBy,groupType_Name,membercount,whenChanged,whenCreated\
| rename cn as "Group Name",groupType_Name as "Type",membercount as "# Members"

#############################################################################################
## Organizational Units Search Macros that point to AD_OU_LDAP_list Lookup:
#############################################################################################
[secrpt-all-orgunits(1)]
args = domain
definition = inputlookup AD_OU_LDAP_list\
| search domain="$domain$"\
| makemv delim="####" Linked_GPO\
| table ou,description,Linked_GPO\
| rename ou as "Name",Linked_GPO as "Linked GPO"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(objectclass=organizationalUnit)" attrs="ou,description,gPLink,gPOptions"|sort ou|rex field=gPLink max_match=10 "\[LDAP://(?<gpo>[^;]+);\d+\]"|ldapfetch dn=gpo attrs=displayName|table ou,description,displayName|rename ou as "Name",displayName as "Linked GPO"

[secrpt-deleted-orgunits(1)]
args = domain
definition = eventtype=admon objectClass="*organizationalUnit*" isDeleted=TRUE|lookup HostToDomain host|search src_nt_domain="$domain$"|dedup objectGUID|table _time,ou,description|rename ou as "Name"

[secrpt-deleted-orgunits_ms_ad_obj(3)]
definition = inputlookup AD_OU_LDAP_list\
| search domain="$domain$" isDeleted="TRUE"\
| eval begintime=strptime("$starttime$", "%m/%d/%y %I:%M %P")\
| eval finishtime=strptime("$endtime$", "%m/%d/%y %I:%M %P")\
| eval whenDeleted=strftime(deletedDate, "%I:%M.%S %P, %a %m/%d/%Y")\
| where deletedDate>begintime AND deletedDate<finishtime\
| makemv delim="####" Linked_GPO\
| table ou,whenDeleted,description,Linked_GPO\
| rename ou as "Name",Linked_GPO as "Linked GPO"
args = domain,starttime,endtime
iseval = 0

[secrpt-gpolinked-orgunits(1)]
args = domain
definition = inputlookup AD_OU_LDAP_list\
| search domain="$domain$" Linked_GPO=* NOT Linked_GPO=""\
| makemv delim="####" Linked_GPO\
| table ou,description,Linked_GPO\
| rename ou as "Name",Linked_GPO as "Linked GPO"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(objectclass=organizationalUnit)" attrs="ou,description,gPLink,gPOptions"|where isnotnull(gPLink)|sort ou|rex field=gPLink max_match=10 "\[LDAP://(?<gpo>[^;]+);\d+\]"|ldapfetch dn=gpo attrs=displayName|table ou,description,displayName|rename ou as "Name",displayName as "Linked GPO"

[secrpt-new-orgunits(1)]
args = domain
definition = eventtype=ms_ad_obj_msad_data (objectClass="top|organizationalUnit") (admonEventType=Update)\
| where uSNChanged==uSNCreated\
| dedup ou\
| join type=left objectGUID [| inputlookup AD_OU_LDAP_list WHERE src_nt_domain="$domain$"\
    | table ou,Linked_GPO]\
| table _time, ou, description, distinguishedName, Linked_GPO\
| rename ou as "Name",Linked_GPO as "Linked GPO"
## Replaced Definition - definition = eventtype=admon objectClass="*organizationalUnit*"|where uSNChanged==uSNCreated|dedup ou|lookup HostToDomain host|search src_nt_domain="$domain$"|ldapfetch dn=distinguishedName attrs="description,gPLink"|rex field=gPLink max_match=10 "\[LDAP://(?<gpo>[^;]+);\d+\]"|lookup ActiveDirectory_GPOInfoLookup distinguishedName as gpo OUTPUT displayName, deletedDate| `format-ad-object-displayname(displayName,deletedDate)`| table _time,ou,description,displayName|rename ou as "Name",displayName as "Linked GPO"

[secrpt-unmanaged-orgunits(1)]
args = domain
definition = inputlookup AD_OU_LDAP_list WHERE domain="$domain$" AND managedBy=""\
| makemv delim="####" Linked_GPO\
| table ou,description,Linked_GPO\
| rename ou as "Name",Linked_GPO as "Linked GPO"
## Replaced Definition - definition = ldapsearch domain="$domain$" search="(&(!(managedBy=*))(objectclass=organizationalUnit))" attrs="ou,description,gPLink,gPOptions"|sort ou|rex field=gPLink max_match=10 "\[LDAP://(?<gpo>[^;]+);\d+\]"|ldapfetch dn=gpo attrs=displayName|table ou,description,displayName|rename ou as "Name",displayName as "Linked GPO"

[secrpt-managed-orgunits(1)]
definition = inputlookup AD_OU_LDAP_list WHERE domain="$domain$" AND managedBy="*" AND NOT managedBy=""\
| makemv delim="####" Linked_GPO\
| table ou,description,Linked_GPO\
| rename ou as "Name",Linked_GPO as "Linked GPO"
args = domain
iseval = 0
##------------------------------------------------------------##
## MS-Windows-AD-Objects - Integration Macros - End Section ##
##------------------------------------------------------------##