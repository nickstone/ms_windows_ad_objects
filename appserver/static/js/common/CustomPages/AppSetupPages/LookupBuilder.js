define([
        'jquery',
        'underscore',
        'splunkjs/mvc/savedsearchmanager',
        'common/statestore/statestorewrapper',
        'common/SearchRunner'
        ],
        function(
            $,
            _,
            SavedSearchManager,
            StateStoreWrapper,
            SearchRunner
        )
{
    var LookupBuilder =  {
        _migratedLookupCollection: 'MSADObj-MigratedLookups',

        buildLookups: function(lookupSearches, parentPage) {
            parentPage.setTitle('Building lookup 1 of ' + lookupSearches.length + '...');
            
            var currentIndex = 0;
            var totalTime = 0;
            var buildNext = function() {
                if (currentIndex < lookupSearches.length) {
                    var currentLookupBuilder = lookupSearches[currentIndex];

                    var lookupSearch = new SavedSearchManager({
                        searchname: currentLookupBuilder,
                        autostart: false
                    });
                    
                    var endProcessed = false;
                    
                    var searchRunner = new SearchRunner(
                        lookupSearch,
                        null,
                        /* search fail handler */ function(message) {
                            parentPage.appendContent('<b>WARNING</b>: ' + currentLookupBuilder + ' could not be built.');
                            currentIndex++;
                            buildNext();
                        },
                        /* search results handler */ function(data) {
                        },
                        /* search start handler */ function() {
                            parentPage.appendContent(currentLookupBuilder + ' ...');
                            var displayIndex = currentIndex + 1;
                            if (displayIndex !== lookupSearches.length) {
                                parentPage.setTitle('Building lookup ' + displayIndex + ' of ' + lookupSearches.length + '...');
                            }
                        },
                        /* search progress handler */ function(isSearchDone, properties) {
                            if (isSearchDone && !endProcessed) {
                                parentPage.appendContent('  - Built.' + 
                            		' (took ' + properties.content.runDuration.toFixed(2) + 's)'
									);
                                currentIndex++;
                                
								totalTime += properties.content.runDuration;
                                endProcessed = true;
                                buildNext();
                            }
                        }
                        );
                    
                    searchRunner.runSearch();
                } else {
                    parentPage.setTitle('Building lookups completed (took ' + totalTime.toFixed(2) + 's).');
                    parentPage.markDone();
                }
            };

            setTimeout(buildNext, 2000);
        }
    };

    return LookupBuilder;
});