/*
 * This file contains a collection of constants for use in the MS Windows AD Objects app's js files
 */

define([
        'splunk.util'
        ],
        function(splunk_util) {
   var MSADObjConstants = {
       getAppName: function() { return 'MS Windows AD Objects'; },
       
       getAppRestId: function() { return 'ms_windows_ad_objects'; },
       
       getDefaultSparklineSettings: function() {
           return {
               type: "line", 
               lineColor: "#070", 
               lineWidth: 1,
               height: 30,
               highlightSpotColor: null, 
               minSpotColor: null, 
               maxSpotColor: null, 
               spotColor: '#070',
               spotRadius: 2,
               fillColor: null
               };
       },

       getSplunkWebUrl: function(){
           return (splunk_util.getConfigValue('MRSPARKLE_ROOT_PATH', '/') + '/')
               .replace(/^(\/)+/, "$1")
               .replace(/(\/)+$/, "$1");
       }
   };
   
   return MSADObjConstants;
});