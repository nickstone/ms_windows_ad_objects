define(function(require, exports, module) {
    var LookupBuilderDialog = require('common/CustomPages/AppSetupPages/LookupBuilderDialog');
    var MSADObjSetupConfig = require('ms_windows_ad_objects/setup/AppSetupConfigs/MSADObjSetupConfig');
    var SetupConfigManager = require('common/CustomPages/AppSetupPages/SetupConfigManager');
    
    SetupConfigManager.build(MSADObjSetupConfig.get());
    LookupBuilderDialog.showLookupBuilderDialog(SetupConfigManager.getLookupBuilders());
});