/*
 * This file contains the code to implement the finish page for setup
 */

requirejs.config({
    enforceDefine: false,
    paths: {
        'text': './contrib/text'
    }
});

define([
        'jquery',
        'underscore',
        'common/CustomPages/AppSetupPages/AppSetupManager',
        'common/CustomPages/AppSetupPages/SetupConfigManager',
        'ms_windows_ad_objects/setup/AppSetupConfigs/MSADObjSetupConfig',
        'text!ms_windows_ad_objects/setup/finish_page.html',
        'ms_windows_ad_objects/MSADObjConstants'
        ],
        function(
            $,
            _,
            AppSetupManager,
            SetupConfigManager,
            MSADObjSetupConfig,
            PageMarkup,
            MSADObjConstants
            )
{
    var webUrl = MSADObjConstants.getSplunkWebUrl();

    var finish_page = {
        addPageParts: function(parentEl) {
            parentEl.html(PageMarkup.replace(/\/dj\//g, webUrl + 'dj/'));
            
            SetupConfigManager.build(MSADObjSetupConfig.get());
            
            AppSetupManager.getNav(SetupConfigManager, function(nav) {
                // Disable links to features in sections that were removed from nav
                // by admin
                var sectionShortcuts = [
                    {section: 'Windows', linkSel: '#windows-home'},
                    {section: 'Active Directory', linkSel: '#ad-home'}
                ];
                
                _.each(sectionShortcuts, function(sectionShortcut) {
                    if (!nav.isSectionInNav(sectionShortcut.section)) {
                        $(sectionShortcut.linkSel).addClass('disabled');
                    }
                });
            });
        },
        
        getPageLabel: function() {
            return 'Finish';
        },
        
        getNextLabel: function() {
            return 'Done';
        },

        validateNext: function() {
            // Next should not be allowed on this page
            return false;
        }
    };
    
    return finish_page;
});