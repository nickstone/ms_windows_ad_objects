define(['ms_windows_ad_objects/MSADObjConstants'],
        function(MSADObjConstants) {
    var MSADObjSetupConfig = {
        get: function() {
            return {
                'appInfo': {
                    'appName': MSADObjConstants.getAppName(),
                    'appRestId': MSADObjConstants.getAppRestId()
                },
                'lookupBuilders': [
                    this.getLookupBuilders()
                ]
            };
        },
        
        getLookupBuilders: function() {
            return [
            	'Build AD Domain Selector',
				'Build AD Initial UAC Details',
                'Build AD Group Lookup',
                'Build AD Group Lookup - Deleted',
                'Build AD Group - Group Membership',                
                'Build AD User Lookup',
                'Build AD User Lookup - Deleted',                
                'Build AD User - Group Membership',
                'Build AD Computer Lookup',
                'Build AD Computer Lookup - Deleted',                
                'Build AD Computer - Group Membership',
                'Build AD DL Group Lookup',
                'Build AD OU Lookup',
                'Build AD OU Lookup - Deleted',                
                'Build AD GPO Lookup',
                'Build AD GPO Lookup - Deleted',
                'Build AD Admin Audit Lookup',
                'Build AD UAC Details Lookup'
                ];
        },

    }    
    return MSADObjSetupConfig;
});